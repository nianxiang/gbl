﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MealController : MonoBehaviour {
	public Text name;
	public Text price;

	public void setSingleMeal(Meal meal){
		this.name.text = meal.name;
		this.price.text = meal.price.ToString ();
	}
}
