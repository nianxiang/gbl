﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrderController : MonoBehaviour {
	public Text name;
	public Text count;

	public void setSingleMeal(OrderMeal order_meal){
		this.name.text = order_meal.getMeal().name;
		this.count.text = order_meal.getCount().ToString();
	}
}
