﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MenuController : MonoBehaviour {
	private UIController ui_controller;
	public GameObject single_meal_prefab;
	private GameObject meal_list_content;
	// Use this for initialization
	void Start () {
		this.ui_controller = GameObject.FindGameObjectWithTag ("UIController").GetComponent<UIController> ();
		this.meal_list_content = GameObject.FindGameObjectWithTag ("MenuListContent").gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void set_menu_name(Button btn){
		this.ui_controller.getUISetter ().add_menu_name.text = btn.name;
	}
	public void set_menu_price(Text price){
		this.ui_controller.getUISetter ().add_menu_price.text = price.text;
	}
	public void add_new_meal(){
		string name = this.ui_controller.getUISetter ().add_menu_name.text;
		string price = this.ui_controller.getUISetter ().add_menu_price.text;
		Meal meal = new Meal (name, price);
		Menu.meal_list.Add (meal);
		foreach (Transform child in this.meal_list_content.transform) {
			GameObject.Destroy (child.gameObject);
		}
		foreach (Meal m in Menu.meal_list) {
			GameObject new_meal = Instantiate (single_meal_prefab) as GameObject;
			MealController mc = new_meal.GetComponent<MealController>();
			mc.setSingleMeal (m);
			new_meal.transform.parent = this.meal_list_content.transform;
			new_meal.transform.localScale = Vector3.one;
			//Debug.Log ("品名： " + m.name + " , 價格： " + m.price);
		}
		GameObject obj = new GameObject ();
		obj = GameObject.Find (name).gameObject;
		Destroy (obj);
	}
}
