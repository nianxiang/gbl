﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UISetter : MonoBehaviour {
	// 對話視窗元件
	public Text chat_name;
	public Text chat_content;
	private GameObject chatUI;

	// 新增菜單視窗元件
	public Text add_menu_name;
	public Text add_menu_price;
	private GameObject menuSetterUI;
	private GameObject menuSetterTriggerBtn;

	// 菜單列表元件
	private GameObject menuUI;
	private GameObject menuUITrigger;
	public GameObject single_meal_prefab;
	private GameObject meal_list_content;

	// 訂單列表元件
	private GameObject orderUI;
	public GameObject single_order_prefab;
	private GameObject order_list_content;
	private Text order_value;
	// 下一階段視窗
	private GameObject passUI;

	// 餐桌列表
	public List<Table> table_list;



	void Start(){
		this.chatUI = GameObject.FindGameObjectWithTag ("ChatUI").gameObject;
		this.menuSetterUI = GameObject.FindGameObjectWithTag ("MenuSetterUI").gameObject;
		this.menuSetterTriggerBtn = GameObject.FindGameObjectWithTag ("MenuSetterTriggerBtn").gameObject;
		this.menuUI = GameObject.FindGameObjectWithTag ("Menu").gameObject;
		this.menuUITrigger = GameObject.FindGameObjectWithTag ("MenuUITrigger").gameObject;
		this.passUI = GameObject.FindGameObjectWithTag ("PassUI").gameObject;
		try{

			this.orderUI = GameObject.FindGameObjectWithTag ("Order").gameObject;
			this.order_list_content = GameObject.FindGameObjectWithTag ("OrderListContent").gameObject;
			this.meal_list_content = GameObject.FindGameObjectWithTag ("MenuListContent").gameObject;
			this.order_value = GameObject.Find ("OrderPrice").GetComponent<Text> ();
			this.orderUI.SetActive (false);
		}catch(System.Exception e){
		
		}
		this.chatUI.SetActive (false);
		this.menuSetterUI.SetActive (false);
		this.menuUI.SetActive (false);
		this.menuSetterTriggerBtn.SetActive (false);
		this.menuUITrigger.SetActive (false);
		this.passUI.SetActive (false);

	}
	public GameObject getChatUI(){
		return this.chatUI;
	}
	public GameObject getMenuSetterUI(){
		return this.menuSetterUI;
	}
	public GameObject getMenuSetterTriggerBtn(){
		return this.menuSetterTriggerBtn;
	}
	public GameObject getOrderUI(){
		return this.orderUI;
	}
	public GameObject getMenuUI(){
		return this.menuUI;
	}
	public GameObject getMenuUITrigger(){
		return this.menuUITrigger;
	}
	public GameObject getPassUI(){
		return this.passUI;
	}
	public List<Table> getTableList(){
		return this.table_list;
	}
	public Text getOrderValue(){
		return this.order_value;
	}
	public Table getActiveTable(){
		foreach (Table t in this.table_list) {
			if (t.is_active ()) {
				return t;
			}
		}
		return new Table ();
	}
	public void set_meal(){
		foreach (Transform child in this.meal_list_content.transform) {
			GameObject.Destroy (child.gameObject);
		}
		foreach (Meal m in Menu.meal_list) {
			GameObject new_meal = Instantiate (single_meal_prefab) as GameObject;
			MealController mc = new_meal.GetComponent<MealController>();
			mc.setSingleMeal (m);
			new_meal.transform.parent = this.meal_list_content.transform;
			new_meal.transform.localScale = Vector3.one;
		}
	}
	public void set_order(List<OrderMeal> order_meal){
		Debug.Log (QuestionController.total_price);
		foreach (Transform child in this.order_list_content.transform) {
			GameObject.Destroy (child.gameObject);
		}
		foreach (OrderMeal m in order_meal) {
			GameObject new_meal = Instantiate (single_order_prefab) as GameObject;
			OrderController oc = new_meal.GetComponent<OrderController>();
			oc.setSingleMeal (m);
			new_meal.transform.parent = this.order_list_content.transform;
			new_meal.transform.localScale = Vector3.one;
		}
	}
}
