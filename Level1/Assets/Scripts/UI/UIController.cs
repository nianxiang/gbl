﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIController : MonoBehaviour {
	private UISetter ui_setter;
	// Use this for initialization
	void Start () {
		this.ui_setter = this.GetComponent<UISetter> ();
		try{
			this.ui_setter.set_meal ();	
		}catch(System.Exception ex){
		
		}
		Screen.SetResolution (1280, 1024, true);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void setChatUI(Chat chat){
		this.ui_setter.chat_name.text = chat.speaker;
		this.ui_setter.chat_content.text = chat.content;
	}
	public void triggerChatUI(bool is_open){
		ui_setter.getChatUI ().SetActive (is_open);
	}
	public void triggerMenuSetterUI(bool is_open){
		ui_setter.getMenuSetterUI ().SetActive (is_open);
	}
	public void triggerMenuSetterTriggerBtn(bool is_open){
		ui_setter.getMenuSetterTriggerBtn ().SetActive (is_open);
	}
	public void triggerMenuUI(bool is_open){
		ui_setter.getMenuUI ().SetActive (is_open);
	}
	public void triggerMenuUITrigger(bool is_open){
		ui_setter.getMenuUITrigger ().SetActive (is_open);
	}
	public void triggerPassUI(bool is_open){
		ui_setter.getPassUI ().SetActive (is_open);
	}
	public void triggerOrderUI(bool is_open){
		ui_setter.getOrderUI ().SetActive (is_open);
	}
	public void destroyUI(GameObject obj){
		Destroy (obj);
	}
	public UISetter getUISetter(){
		return this.ui_setter;
	}
	public int getOrderPrice(){
		int value = 0;
		int.TryParse (this.ui_setter.getOrderValue ().text, out value);
		return value;
	}
	public void toNextLevel(){
		Application.LoadLevel (Application.loadedLevel + 1);
	}
	public void set_order_UI(List<OrderMeal> order_meal){
		this.ui_setter.set_order (order_meal);
	}

	public void random_table(){
		foreach (Table t in this.ui_setter.table_list) {
			t.trigger_table (false);
		}
		int random_value = Random.Range (0, this.ui_setter.table_list.Count);
		this.ui_setter.table_list [random_value].trigger_table (true);
	}
}
