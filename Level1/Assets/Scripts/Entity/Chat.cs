﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class Chat {
	public string speaker;
	public string content;
	public string userPhoto;
	public Chat(string speaker , string content){
		this.speaker = speaker;
		this.content = content;
	}
}
