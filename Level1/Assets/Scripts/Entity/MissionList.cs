﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System.IO;
using System;
using System.Text;

public class MissionList : MonoBehaviour
{
	public List<Mission> mission_list = new List<Mission> ();
	public string mission_number;
	// Use this for initialization

	IEnumerator Start ()
	{
		WWW www = new WWW ("http://140.120.54.114:5000/API/Game/Get/Level/" +  mission_number);
		//WWW www = new WWW ("http://140.120.54.104:8001/programplatform/platform/public/api/GetMission/" + mission_number);
		yield return www;
		string json_data = "";
		if (www.error == null) {
			setMissionList(www.text);
		}
		foreach (Mission m in this.mission_list) {
			foreach (Chat chat_data in m.chat_list) {
				//Debug.Log ("speaker = " + chat_data.speaker + " , content = " + chat_data.content);
			}
		}
	}
	public void setMissionList(string data){
		JSONObject output = new JSONObject (data);
		foreach (JSONObject j_data in output.list) {
			string name = j_data ["name"].str;

			string trigger = (string)j_data ["trigger"].str;
			string end_trigger = (string)j_data ["end_trigger"].str;
			JSONObject chat_list = j_data ["chat"];
			Mission mission = new Mission (name, trigger, end_trigger);
			foreach (JSONObject chat in chat_list.list) {
				string speaker = chat ["speaker"].str;
				string content = chat ["content"].str;
				speaker = UnicodeToString (@speaker);
				content = UnicodeToString (@content);
				//Debug.Log(UnicodeToString(@content));
				Chat chat_data = new Chat (speaker , content);
				//Chat chat_data = new Chat ("test", (string)chat ["content"]);
				//Debug.Log ("speaker = " + chat_data.speaker + " , content = " + chat_data.content);
				mission.chat_list.Add (chat_data);
			}
			this.mission_list.Add (mission);
		}

	}
	/*public void setMissionList(string data){
		JsonData json_data = JsonMapper.ToObject<JsonData> (data);
		foreach (JsonData j_data in json_data) {
			string name = (string)j_data ["name"];
			//string trigger = (string)j_data ["trigger"];
			//string end_trigger = (string)j_data ["end_trigger"];
			string trigger = "NPC";
			string end_trigger = "Computer";
			Mission mission = new Mission (name, trigger , end_trigger);
			if (j_data ["chat"].ToJson() != "") {
				if (j_data ["chat"].Count > 0) {
					foreach (JsonData chat in j_data ["chat"]) {
						//Chat chat_data = new Chat ((string)chat ["uid"],(string)chat ["content"]);
						Chat chat_data = new Chat ("test",(string)chat ["content"]);
						//Debug.Log ("speaker = " + chat_data.speaker + " , content = " + chat_data.content);
						mission.chat_list.Add (chat_data);
					}
				}
				this.mission_list.Add (mission);

			}

		}

	}*/
	// Update is called once per frame
	void Update ()
	{
	
	}
	public string enc(byte[] data){
		byte[] bc = UnicodeEncoding.Convert (Encoding.UTF8, Encoding.Unicode, data);
		return Encoding.Unicode.GetString (bc);
	}
	public string Enc(string input){
		if (input == null || "".Equals (input)) {
			return "";
		}
		StringBuilder sb = new StringBuilder ();
		byte[] byStr = Encoding.UTF8.GetBytes (input);
		for (int i = 0; i < byStr.Length; i++) {
			sb.Append (@"%" + Convert.ToString (byStr [i], 16));
		}
		return (sb.ToString ());
	}
	private string UnicodeToString(string data){
		string dst = "";
		string src = data;
		int len = data.Length / 6;
		try{
			for (int i = 0; i <= len - 1; i++) 
			{ 
				string str = ""; 
				str = src.Substring(0, 6).Substring(2); 
				src = src.Substring(6); 
				byte[] bytes = new byte[2]; 
				bytes[1] = byte.Parse(int.Parse(str.Substring(0,2),System.Globalization.NumberStyles.HexNumber).ToString()); 
				bytes[0] = byte.Parse(int.Parse(str.Substring(2, 2),System.Globalization.NumberStyles.HexNumber).ToString()); 
				dst += Encoding.Unicode.GetString(bytes);
			} 	
			return dst; 
		}catch(Exception e){
			return data;
		}

	}  
}

