﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TimeHandler : MonoBehaviour {
	private float timeLeft = 0;
	private bool is_over = true;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!is_over) {
			count_down ();
		}
	}
	public void count_down(Text timer){
		this.timeLeft -= Time.deltaTime;
		timer.text = Mathf.Round (timeLeft).ToString();
	}
	public void set_time_hander(float time){
		this.timeLeft = time;
		this.is_over = false;
	}
	public void count_down(){
		this.timeLeft -= Time.deltaTime;
		if (timeLeft < 0) {
			is_over = true;
		} else {
			Debug.Log (Mathf.Round (timeLeft));
		}
	}
}
