﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Table : MonoBehaviour {
	private GameObject alert;
	private bool is_open;
	// Use this for initialization
	void Start () {
		this.alert = this.transform.FindChild("TableAlert").gameObject;
		//this.box_collider = this.GetComponent<BoxCollider2D> ();
		this.alert.SetActive (false);
	}
	public void trigger_table(bool is_open){
		this.alert.SetActive (is_open);
		this.is_open = is_open;
	}
	public bool is_active(){
		return this.is_open;
	}
}
