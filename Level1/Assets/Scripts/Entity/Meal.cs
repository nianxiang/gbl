﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meal{
	public string name;
	public int price;
	public Meal(string name, string price){
		this.name = name;
		this.price = int.Parse (price);
	}
}
