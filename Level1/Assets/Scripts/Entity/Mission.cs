﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System.IO;

public class Mission : MonoBehaviour {
	public List<Chat> chat_list = new List<Chat> ();
	public string trigger;
	public string end_trigger;
	public string mission_id;
	public string name;
	public Mission(string name , string trigger , string end_trigger){
		this.name = name;
		this.trigger = trigger;
		this.end_trigger = end_trigger;
	}
	//IEnumerator Start(){
		//WWW www = new WWW ("http://140.120.54.114:5000/API/Game/Get/Mission/" + mission_id);
		//yield return www;
		/*if (www.error == null) {
			setChatList(www.text);
		}*/
	//}
	public void setChatList(string data){
		JsonData json_data = new JsonData ();
		json_data = JsonMapper.ToObject<JsonData> (data);
		/*foreach (JsonData j_data in json_data) {
			string name = (string)j_data ["name"];
			Mission mission = new Mission ();
			foreach (JsonData chat in j_data ["chat_list"]) {
				string speaker = (string)chat ["speaker"];
				string content = (string)chat ["content"];
				Chat chat_data = new Chat ();
				chat_data.speaker = speaker;
				chat_data.content = content;
				this.chat_list.Add (chat_data);
			}
		}*/
		//foreach (JsonData chat in json_data) {
		for(int i = 0 ; i < json_data.Count ; i++){
			JsonData chat = new JsonData ();
			chat = json_data [i];
			string speaker = (string)chat ["speaker"];
			string content = (string)chat ["content"];
			Chat chat_data = new Chat (speaker,content);
			chat_data.speaker = speaker;
			chat_data.content = content;
			this.chat_list.Add (chat_data);
		}
	}
	public List<Chat> getChatList(){
		return this.chat_list;
	}
	public int getMissionCount(){
		return this.chat_list.Count + 1;
	}
	public List<Chat> getMission(int step){
		return this.chat_list;
	}
}
