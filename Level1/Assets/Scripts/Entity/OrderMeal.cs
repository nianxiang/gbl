﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderMeal {
	private Meal meal;
	private int count;
	public OrderMeal(Meal meal , int count){
		this.meal = meal;
		this.count = count;
	}
	public Meal getMeal(){
		return this.meal;
	}
	public int getCount(){
		return this.count;
	}
	public int getTotalPrice(){
		return this.meal.price * this.count;
	}
}
