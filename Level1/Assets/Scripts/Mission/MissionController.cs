﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class MissionController : MonoBehaviour {
	protected MissionList mission_list;
	protected int current_mission_step;
	protected int current_chat_step;
	public bool mission_is_over , step_is_over , is_trigger, mission_complete;
	public UIController ui_controller;
	protected PlayerController play_controller;
	// Use this for initialization
	protected void Start () {
		this.current_chat_step = 0;
		this.current_mission_step = 0;
		this.mission_is_over = false;
		this.step_is_over = true;
		this.is_trigger = false;
		this.mission_complete = true;
		this.mission_list = this.GetComponent<MissionList> ();
		this.ui_controller = GameObject.FindGameObjectWithTag ("UIController").GetComponent<UIController> ();
		this.play_controller = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController> ();
	}

	// Update is called once per frame
	protected void Update () {
		process_ui_control ();
		process_condition_control ();
		if (!mission_is_over && !step_is_over && mission_complete) {
			play_controller.set_player_option (false);
			ui_controller.triggerChatUI (true);
			int max_mission_count = mission_list.mission_list.Count;
			int max_chat_count = mission_list.mission_list [current_mission_step].chat_list.Count;
			if (current_chat_step < max_chat_count) {
				Chat chat_data = mission_list.mission_list [current_mission_step].chat_list [current_chat_step];
				ui_controller.setChatUI (chat_data);
				if (Input.GetMouseButtonDown (0)) {
					current_chat_step += 1;
				}
			} else {
				ui_controller.triggerChatUI (false);
				step_is_over = true;
				current_chat_step = 0;
				current_mission_step += 1;
				if (current_mission_step >= max_mission_count) {
					mission_is_over = !mission_is_over;
					ui_controller.triggerPassUI (true);
					step_is_over = true;
				}
			}
		} else {
			ui_controller.triggerChatUI (false);
			play_controller.set_player_option (true);
		}
	}
	public void trigger_object(GameObject obj){
		trigger_obj (obj);
	}
	protected virtual void trigger_obj(GameObject obj){
		string current_mission_name = mission_list.mission_list [current_mission_step].trigger;
		if (current_mission_name == obj.tag && mission_complete) {
			step_is_over = false;
		}
	}
	public string get_current_trigger_name(){
		try{
			if (!mission_is_over) {
				return mission_list.mission_list [current_mission_step].trigger;
			}else{
				return "game over";
			}
		}catch(Exception e){
			return "Game over";
		}
	}
	protected virtual void process_ui_control(){
		switch (current_mission_step) {
		case 0:
			break;
		case 1:
			ui_controller.triggerMenuSetterTriggerBtn (true);
			break;
		case 2:
			ui_controller.triggerMenuUITrigger (true);
			break;
		case 3:
			ui_controller.triggerMenuSetterUI (false);
			break;
		default:
			break;
		}
	}
	protected virtual void process_condition_control(){
		switch (current_mission_step) {
		case 1:
			if (Menu.meal_list.Count > 0) {
				mission_complete = true;
				ui_controller.triggerMenuSetterUI (false);
				step_is_over = false;
			} else {
				mission_complete = false;
			}
			break;
		case 2:
			if (Menu.meal_list.Count > 4) {
				mission_complete = true;
			} else {
				mission_complete = false;
			}
			break;
		case 3:
			break;
		default:
			break;
		}
	}
}
