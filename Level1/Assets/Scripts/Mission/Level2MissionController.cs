﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level2MissionController : MissionController {
	TimeHandler time_handler = new TimeHandler();
	private bool is_change_table = false;
	static Random rd = new Random ();
	// Use this for initialization
	void Start () {
		base.Start ();
		//test_data ();

		/*foreach (Meal m in Menu.meal_list) {
			Debug.Log ("name = " + m.name + " , price = " + m.price);
		}*/

		//time_handler.set_time_hander (5);
	}
	
	// Update is called once per frame
	void Update () {
		base.Update ();
		time_handler.count_down ();
	}

	public void trigger_object(GameObject obj){
		base.trigger_object (obj);
	}
	public void get_current_trigger_name(){
		base.get_current_trigger_name ();
	}
	protected override void process_ui_control(){
		Debug.Log (current_mission_step);
		switch (current_mission_step) {
		case 0:
			ui_controller.triggerMenuUITrigger (true);
			break;
		case 1:
			
			break;
		case 2:
			if (!is_change_table) {
				this.ui_controller.random_table();
				is_change_table = true;
			}
			break;
		case 3:
			if (!is_change_table) {
				this.ui_controller.random_table();
				is_change_table = true;
			}
			break;
		case 4:
			if (QuestionController.correct_count > 4) {
				step_is_over = false;
			} else {
				step_is_over = true;
			}
			break;
		default:
			break;
		}
	}
	protected override void process_condition_control(){
		switch (current_mission_step) {
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;
		default:
			break;
		}
	}
	protected override void trigger_obj(GameObject obj){
		Debug.Log (current_mission_step);
		string current_mission_name = mission_list.mission_list [current_mission_step].trigger;
		/*if (current_mission_name == obj.tag && mission_complete) {
			step_is_over = false;
		}*/
		switch (current_mission_step) {
		case 2:
			if (obj.name == this.ui_controller.getUISetter ().getActiveTable ().name) {
				QuestionController qc = new QuestionController ();
				this.ui_controller.set_order_UI (qc.get_ramdom_menu ());
				this.ui_controller.triggerOrderUI (true);
				step_is_over = false;
			}
			break;
		case 3:
			
			break;
		case 4:
			if (obj.name == this.ui_controller.getUISetter ().getActiveTable ().name) {
				QuestionController qc = new QuestionController ();
				this.ui_controller.set_order_UI (qc.get_ramdom_menu ());
				this.ui_controller.triggerOrderUI (true);
			}
			break;
		default:
			if (current_mission_name == obj.tag && mission_complete) {
				step_is_over = false;
			}
			break;
		}
	}
	public void check_order_price_is_correct(){
		int input_value = this.ui_controller.getOrderPrice ();
		if (input_value == QuestionController.total_price) {
			submit_order_price (true);
		} else {
			submit_order_price (false);
		}
	}
	private void submit_order_price(bool is_correct){
		if (is_correct) {
			this.ui_controller.triggerOrderUI (false);

			switch (current_mission_step) {
			case 3:
				step_is_over = false;
				break;
			case 4:
				QuestionController.correct_count += 1;

				is_change_table = false;
				if (!is_change_table) {
					this.ui_controller.random_table();
					is_change_table = true;
				}
				break;
			default:
				break;
			}
		} else {
		
		}
	}

	protected void test_data(){
		Meal meal1 = new Meal ("水餃", "50");
		Menu.meal_list.Add (meal1);
		Meal meal2 = new Meal ("玉米濃湯", "40");
		Menu.meal_list.Add (meal2);
		Meal meal3 = new Meal ("牛肉麵", "100");
		Menu.meal_list.Add (meal3);
		Meal meal4 = new Meal ("烤肉飯", "40");
		Menu.meal_list.Add (meal4);
		Meal meal5 = new Meal ("雞肉飯", "60");
		Menu.meal_list.Add (meal5);
	}
}
