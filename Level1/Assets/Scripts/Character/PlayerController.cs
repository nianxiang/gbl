﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	private Animator animator;
	public int move_speed = 1;
	public int play_level;
	private MissionController mission_controller;
	private bool allow_play;
	// Use this for initialization
	void Start () {
		this.allow_play = true;
		switch_mission_controller ();
		animator = this.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (this.allow_play) {
			var vertical = Input.GetAxis ("Vertical");
			var horizontal = Input.GetAxis ("Horizontal");

			if (vertical > 0) {
				animator.SetInteger ("direction", 1);
				transform.Translate (Vector2.up * move_speed * Time.deltaTime);
			} else if (vertical < 0) {
				animator.SetInteger ("direction", 2);
				transform.Translate (Vector2.down * move_speed * Time.deltaTime);
			} else if (horizontal < 0) {
				animator.SetInteger ("direction", 3);
				transform.Translate (Vector2.left * move_speed * Time.deltaTime);
			} else if (horizontal > 0) {
				animator.SetInteger ("direction", 4);
				transform.Translate (Vector2.right * move_speed * Time.deltaTime);
			} else {
				animator.SetInteger ("direction", 0);
			}
		}
	}
	void OnTriggerEnter2D(Collider2D other) 
	{
		this.mission_controller.trigger_object (other.gameObject);
	}
	public void set_player_option(bool allow){
		this.allow_play = allow;
	}
	public void switch_mission_controller(){
		switch (this.play_level) {
		case 0:
			this.mission_controller = GameObject.FindGameObjectWithTag ("MissionController").GetComponent<MissionController> ();
			break;
		case 1:
			this.mission_controller = GameObject.FindGameObjectWithTag ("MissionController").GetComponent<Level2MissionController> ();
			break;
		default:
			break;
		}
	}
}
