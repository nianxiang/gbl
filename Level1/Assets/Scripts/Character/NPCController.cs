﻿using UnityEngine;
using System.Collections;

public class NPCController : MonoBehaviour
{
	private MissionController mission_controller;
	public GameObject triggerAlert;
	// Use this for initialization
	void Start ()
	{
		this.mission_controller = GameObject.FindGameObjectWithTag ("MissionController").GetComponent<MissionController> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		is_trigger ();
	}
	public void is_trigger(){
		if (this.tag == this.mission_controller.get_current_trigger_name ()) {
			triggerAlert.SetActive (true);
		} else {
			triggerAlert.SetActive (false);
		}
	}
}

