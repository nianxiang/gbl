﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionController : MonoBehaviour {
	private List<List<OrderMeal>> order_list = new List<List<OrderMeal>>();
	public static int total_price = 0;
	public static int correct_count = 0;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	protected int get_random_number(int min_number ,int max_number){
		int random_value = Random.Range (min_number, max_number);
		return random_value;
	}
	public List<OrderMeal> get_ramdom_menu(){
		List<OrderMeal> order_temp = new List<OrderMeal>();
		QuestionController.total_price = 0;
		int total_price = 0;
		for (int i = 0; i < 3; i++) {
			Meal m = new Meal ("test", "100");
			m = Menu.meal_list[Random.Range(0 , Menu.meal_list.Count -1 )];
			int count = this.get_random_number (1,11);
			OrderMeal om = new OrderMeal (m, count);
			order_temp.Add (om);
			QuestionController.total_price += om.getTotalPrice ();
		}
		return order_temp;
	}
}
